const answers = ["It is certain", 
                   "It seems so", 
                   "Without a doubt", 
                   "Yes - definitely",
                   "You can rely on it", 
                   "Magic says, yes", 
                   "Probably, ask again later", 
                   "Doesn't look good", 
                   "Yes", "Signs point to yes",
                   "Don't count on it", 
                   "no",
                   "My minds telling me nooooo", 
                   "Outlook not so good",
                   "Very doubtful", 
                   "Reply hazy, try again", 
                   "Ask again later", 
                   "Better not tell you now",
                   "Cannot predict now", 
                   "Concentrate and ask again"];
    
    window.onload = function() {
       const eight = document.getElementById("eight");
       const answer = document.getElementById("answer");
       const eightball = document.getElementById("eightball");
       const question = document.getElementById("question");
    
       eightball.addEventListener("click", function() {
         if (question.value.length < 1) {
           alert('Enter a question!');
         } else {
           eight.innerText = "";
           const num = Math.floor(Math.random() * Math.floor(answers.length));
           answer.innerText = answers[num];
         }
       });
    };